from discord import Game, Client, Guild
from discord.ext import commands
import discord.utils as utils
import discord.opus as opus

import time, asyncio
import logging

from threading import Timer

from quiz import Quiz
from game_instance import GameInstance

import yaml

logging.basicConfig(level=logging.INFO)
# load_opus_lib()

# retrieving bot token from config file
config_file_path = "config.yaml"
with open(config_file_path, "r") as config_file:
    data = yaml.load(config_file)

TOKEN = data["token"]

bot = commands.Bot(command_prefix="!")

game_instances = {}


def get_game_instance(guild: Guild):
    # if guild.id not in game_instances:
    if not game_instances.get(guild.id):
        game = GameInstance(bot)
        game_instances[guild.id] = game
        bot.loop.create_task(game.game_loop_update())
        return game
    else:
        return game_instances[guild.id]


@bot.command()
async def test(ctx, *args):
    game = get_game_instance(ctx.guild)
    print("test command called by %s" % ctx.author)
    await game.load(ctx, *args)
    await game.setup(ctx, *args)
    await game.start(ctx, *args)


@bot.command()
async def load(ctx, *args):
    game = get_game_instance(ctx.guild)
    print("load command called by %s" % ctx.author)
    await game.load(ctx, *args)


@bot.command()
async def setup(ctx, *args):
    game = get_game_instance(ctx.guild)
    print("setup command called by %s" % ctx.author)
    await game.setup(ctx, *args)


@bot.command()
async def start(ctx, *args):
    game = get_game_instance(ctx.guild)
    print("start command called by %s" % ctx.author)
    await game.start(ctx, *args)


@bot.command()
async def stop(ctx, *args):
    game = get_game_instance(ctx.guild)
    print("stop command called by %s" % ctx.author)
    await game.stop(ctx, *args)


@bot.command()
async def scores(ctx, *args):
    game = get_game_instance(ctx.guild)
    print("scores command called by %s" % ctx.author)
    await game.scores(ctx, *args)


# for testing purposes
# TODO: make only the bot creator (dev) can use this command
@bot.command()
async def terminate(ctx, *args):
    print("terminate command called by %s" % ctx.author)
    await bot.logout()


# @test.error
# async def test_error(error, ctx):
#     print(error)
#     if isinstance(error, commands.BadArgument):
#         await bot.send_message(ctx.message.channel, error)
#     await bot.send_message(ctx.message.channel, "im not ok plz halp")


@bot.event
async def on_ready():
    print("Bot is ready")
    await bot.change_presence(activity=Game(name="Testing the bot lel"))


@bot.event
async def on_message(message):
    if message.author.bot:
        return

    game = get_game_instance(message.guild)
    await bot.process_commands(message)
    await game.check_guessing(message)


@bot.event
async def on_voice_state_update(member, before, after):
    if member.bot:
        return
    voice_client = utils.get(bot.voice_clients, guild=member.guild)

    if voice_client is None:
        return

    if after.channel == voice_client.channel:
        game = get_game_instance(member.guild)
        await game.add_participant(member)


bot.run(TOKEN)


print("stopped running lel")

