from typing import Dict

from discord import CategoryChannel, Guild, TextChannel

import discord.utils as utils


class Tools:

    guild: Guild = None

    def __init__(self, guild: Guild):
        Tools.guild = guild

    @staticmethod
    async def get_or_create_category(guild: Guild, name: str) -> CategoryChannel:
        category = utils.get(guild.categories, name=name)

        if category is not None:
            return category

        await Tools.guild.create_category(name)

        category = utils.get(guild.categories, name=name)
        return category

    @staticmethod
    async def get_or_create_TextChannel(
        from_category: CategoryChannel, name: str, overwrites: Dict
    ) -> TextChannel:
        channel = utils.get(from_category.text_channels, name=name)

        if channel is not None:
            return channel

        await Tools.guild.create_text_channel(
            name, category=from_category, overwrites=overwrites
        )

        channel = utils.get(from_category.text_channels, name=name)
        return channel

