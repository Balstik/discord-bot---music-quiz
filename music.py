import youtube_dl
import requests
import io

from mimetypes import guess_extension

from answers import Answers
from settings import Settings
from discord import File

ydl_opts = {
    # "no_warnings": True,
    # "quiet": True,
    "format": "bestaudio/best",
    "noplaylist": True,
    "postprocessors": [
        {
            "key": "FFmpegExtractAudio",
            "preferredcodec": "mp3",
            "preferredquality": "192",
        }
    ],
}


class URLNotSpecifiedError(Exception):
    pass


class Music:
    def __init__(self, infos: dict, settings: Settings):
        self.name: str = infos.get("name", None)
        self.author: str = infos.get("author", None)
        self.work: str = infos.get("from", None)
        self.points: int = int(infos.get("points", 1))

        self.url: str = infos.get("url", None)

        self.image_url: str = infos.get("image_url", None)
        self.thumbnail_url: str = None
        self.image_file: File = None

        # if "accepted_answers" in infos:
        if infos.get("accepted_answers"):
            accepted_answers = infos["accepted_answers"]
        else:
            accepted_answers = {}
        self.answers = Answers(self.name, self.author, self.work, accepted_answers)

        self.find: str = infos.get("find", settings.find)

        if settings.music_time_type == "fixed":
            self.duration = settings.music_duration
        elif settings.music_time_type == "specified":
            self.duration = infos.get("duration", settings.music_duration)

        self.loadable = True

    def load_image(self):

        self.image_file = self.request_url(self.image_url)

        if not self.image_file:
            self.image_file = self.request_url(self.thumbnail_url)

    def request_url(self, url: str) -> File:
        if url:
            response = requests.get(url)
            if response:
                ext = guess_extension(
                    response.headers["Content-Type"].partition(";")[0].strip(),
                    strict=True,
                )
                # small rectification because dicord does not handle .jpe extension
                if ext == ".jpe":
                    ext = ".jpg"
                print(ext)
                f = io.BytesIO(response.content)

                return File(f, filename="%s%s" % (self.name, ext))
        return None

    def load_music(self):
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            try:
                if self.url is None:
                    raise URLNotSpecifiedError("No url was specified for this music")
                result = ydl.extract_info(self.url, download=False)
            except youtube_dl.utils.DownloadError as error:
                print("An error occured during the download of the music :")
                print(error)
                self.loadable = False
                return None
            except URLNotSpecifiedError as error:
                print(error)
                self.loadable = False
                return None
            else:
                # if "entries" in result:
                if result.get("entries"):
                    # Can be a playlist or a list of videos
                    video = result["entries"][0]
                else:
                    # Just a video
                    video = result
                video_url = video["url"]
                self.thumbnail_url = video["thumbnail"]
                return video_url

    def is_guessable(self) -> bool:
        if self.find == "author":
            return self.author or len(self.answers.accepted_answers) >= 1
        elif self.find == "name":
            return self.name or len(self.answers.accepted_answers) >= 1
        elif self.find == "from":
            return self.work or len(self.answers.accepted_answers) >= 1

        # no find type has been defined
        return False

    def get_answer_format(self) -> str:
        str_list = []
        if self.name:
            str_list.append(" **%s**" % self.name)
        if self.author:
            str_list.append(" by **%s**" % self.author)
        if self.work:
            str_list.append(" from **%s**" % self.work)

        return "".join(str_list)

    def __repr__(self) -> str:
        return "<%s music.Music object>" % (self.name)

