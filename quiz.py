from discord import Message, Member, TextChannel
from discord.ext.commands import Context

import asyncio, yaml
from typing import Dict, List

from playlist import Playlist, Music, Settings
from participant_infos import ParticipantInfos, Participants


class Quiz:
    def __init__(self):
        self.name: str = "No name found"
        self.description: str = "No description found"

        self.playlist: Playlist = Playlist()
        self.settings: Settings = Settings()
        # TODO: delete this bool because of new state pattern
        # self.playing: bool = False

        self.current_music: Music = None

        # self.current_state: QuizState = StoppedState(self)

    def load_from_file(self, filepath: str):
        # TODO : need to check if the file format is valid first
        with open(filepath, "r") as yaml_file:
            data = yaml.load(yaml_file)
            self.init_from_data(data)

    def load_from_bytes(self, content: bytes):
        data = yaml.load(content)
        self.init_from_data(data)

    def init_from_data(self, data: dict):
        self.settings = Settings(data.get("settings", None))
        self.playlist = Playlist(data.get("musics", None), self.settings)
        quiz_infos = data.get("quiz", None)
        if quiz_infos is not None:
            self.name = quiz_infos.get("name", self.name)
            self.description = quiz_infos.get("description", self.description)

    def stop(self):
        # self.playing = False
        self.playlist.stop_playlist()

    def take_guess(self, participant_infos: ParticipantInfos, guess: str) -> bool:
        return self.current_music.answers.guess(
            # guess, self.settings.find, participant_infos.difficulty
            guess,
            self.current_music.find,
            participant_infos.difficulty,
        )

    # async def countdown(self, delay, nb, action, *args):
    #     for i in range(nb, -1, -1):
    #         asyncio.create_task(action(i, *args))
    #         await asyncio.sleep(delay)
    #         if not self.playing:
    #             break

    # async def update_countdown_msg(self, i: int, msg: Message, new_content: str):
    #     if self.playing:
    #         # print("j'écrit... " + str(i))
    #         await msg.edit(content=new_content % i)
    #         # print("c'est bon ! " + str(i))

