import asyncio
from discord import Member, Guild, TextChannel, File

from discord.ext.commands import Bot

import discord.utils as utils

from typing import List, Dict
import time

from tools import Tools
from quiz import Quiz
from participant_infos import ParticipantInfos, Participants
from quiz_states import (
    QuizStateType,
    QuizState,
    LoadedState,
    ReadyState,
    PlayingState,
    PausedState,
    IntervalState,
    EndedState,
    StoppedState,
)


class GameInstance:
    """The class representing an instance of the bot in a particular guild.
    """

    def __init__(self, bot):
        self.quiz: Quiz = None
        self.bot: Bot = bot
        self.guild: Guild = None
        # self.participants_infos: Dict[Member, ParticipantInfos] = {}
        self.participants: Participants = Participants()

        self.current_state: QuizState = StoppedState(self)
        self.is_alone: bool = False
        self.alone_time_before_leaving: float = 60

    def init_from_context(self, ctx):
        if self.guild is None:
            self.guild = ctx.guild
            self.participants.guild = self.guild
            Tools(self.guild)

    async def change_state_to(self, next_state: QuizState, ctx=None, *args):
        if self.current_state is not None:
            await self.current_state.on_state_exit(ctx, *args)
            await self.current_state.transition_to(next_state)
        self.current_state = next_state
        print("change state to " + str(self.get_state_type(next_state)))

        await self.current_state.on_state_enter(ctx, *args)
        self.current_state.start_update()

    async def game_loop_update(self):
        while True:
            if self.current_state.update_start:
                await self.current_state.on_state_update()

            await self.check_alone_time()

            await asyncio.sleep(0.1)

    def get_state_type(self, state: QuizState) -> QuizStateType:
        # TODO: automate this if possible ? (should work with whatever states exist)
        if type(state) is LoadedState:
            return QuizStateType.LOADED
        if type(state) is ReadyState:
            return QuizStateType.READY
        if type(state) is PlayingState:
            return QuizStateType.PLAYING
        if type(state) is PausedState:
            return QuizStateType.PAUSED
        if type(state) is IntervalState:
            return QuizStateType.INTERVAL
        if type(state) is EndedState:
            return QuizStateType.ENDED
        if type(state) is StoppedState:
            return QuizStateType.STOPPED
        # return STOPPED type by default
        return QuizStateType.STOPPED

    def is_state_stopped(self) -> bool:
        return self.get_state_type(self.current_state) == QuizStateType.STOPPED

    def is_current_state(self, state_type: QuizStateType) -> bool:
        return self.get_state_type(self.current_state) == state_type

    async def load(self, ctx, *args):
        self.init_from_context(ctx)

        await self.change_state_to(LoadedState(self), ctx, *args)
        # self.quiz = Quiz()

        # self.quiz.load_from_file("test.yaml")
        # await ctx.send(
        #     "Loaded %s with %s musics" % (self.quiz.name, self.quiz.playlist.nb_music)
        # )

    async def setup(self, ctx, *args):
        self.init_from_context(ctx)

        if self.quiz is None:
            await ctx.send("No quizs currently loaded")
            return

        if not ctx.author.voice:
            await ctx.send("Join a voice channel first so I can join you in")
            return
        print(" on setup")
        await self.change_state_to(ReadyState(self), ctx)
        # # TODO : maybe move voice_client elsewhere ?
        # participants = ctx.author.voice.channel.members
        # self.quiz.playlist.voice_client = await ctx.author.voice.channel.connect()

        # await self.participants.add_and_setup_participants(participants)

    async def start(self, ctx, *args):
        self.init_from_context(ctx)

        # if len(args) >= 1:
        #     print(args[0])
        #     await ctx.send(args[0])

        if self.quiz is None:
            await ctx.send("No quizs currently loaded")
            return

        if self.is_current_state(QuizStateType.PLAYING):
            await ctx.send("A quiz is already playing")
            return

        if not self.is_current_state(QuizStateType.READY):
            await ctx.send("I am not ready yet, have you used '!setup' ?")
            return

        # await self.quiz.start_quiz(self.participants)
        await self.change_state_to(PlayingState(self), ctx)

    async def stop(self, ctx, *args):
        self.init_from_context(ctx)

        if self.quiz is None:
            await ctx.send("No quizs currently loaded")
            return

        if not self.is_current_state(QuizStateType.PLAYING):
            print("quiz is already stopped")
            return

        await self.change_state_to(StoppedState(self), ctx)
        # voice_client = utils.get(self.bot.voice_clients, guild=ctx.guild)
        # self.quiz.stop()
        # print("manually stopping the quiz")
        # if voice_client is not None:
        #     await voice_client.disconnect()

    async def scores(self, ctx, *args):
        self.init_from_context(ctx)

        if self.quiz is None:
            await ctx.send("No quizs currently loaded")
            return

        if not self.is_current_state(QuizStateType.PLAYING):
            await ctx.send("No quizs are playing")
            return

        await ctx.send("Current leaderboards :\n" + self.participants.get_score_str())

    async def add_participant(self, member):
        # self.init_from_context(ctx)
        await self.participants.add_and_setup_participants([member])

    async def end_current_music(self):
        if self.quiz.playlist.music_left:
            await self.change_state_to(IntervalState(self))
        else:
            await self.change_state_to(EndedState(self))

    async def write_to_participants_channels(self, message: str, file: File = None):
        return await self.write_to_channels(
            self.participants.get_answer_channels(), message, file
        )

    def is_alone_in_channel(self):
        voice_client = utils.get(self.bot.voice_clients, guild=self.guild)
        if voice_client:
            if len(voice_client.channel.members) <= 1:
                return True
        return False

    async def check_alone_time(self):
        if self.is_alone_in_channel():
            if not self.is_alone:
                self.alone_time = time.time()
            self.is_alone = True
            if (time.time() - self.alone_time) > self.alone_time_before_leaving:
                await self.write_to_participants_channels(
                    " Stopping the quiz because I was alone"
                )
                await self.change_state_to(StoppedState(self))
        else:
            self.is_alone = False

    def get_points(self) -> int:
        if self.quiz.settings.points_given_type == "same":
            return self.quiz.current_music.points
        elif self.quiz.settings.points_given_type == "fastest":
            return max(
                self.quiz.current_music.points
                - self.participants.get_nb_player_found(self.quiz.playlist.index),
                1,
            )

        return 0

    async def write_to_channels(
        self, channels: List[TextChannel], message: str, file: File = None
    ):
        messages_sent = []
        for channel in channels:
            messages_sent.append(await channel.send(message, file=file))
        return messages_sent

    async def check_guessing(self, message):
        ctx = await self.bot.get_context(message)
        # if message is a command, message is not a guess
        if ctx.command is not None:
            return

        # if no quizs is currently being played
        if not self.is_current_state(QuizStateType.PLAYING):
            return

        author = message.author
        channel = message.channel

        # Check if the user is a participant
        if author in self.participants:
            # Check if the user is guessing in the channel it was attributed to them
            if self.participants.get_infos(author).answer_channel == channel:

                participant_infos = self.participants.get_infos(author)
                # Check if the guess is a correct answer
                if self.quiz.take_guess(participant_infos, message.content):
                    # Check if the participant did not already give the correct answer
                    if not participant_infos.results.get(self.quiz.playlist.index):
                        # TODO : make winning more or less points based on participant difficulty and gamemode
                        # participant_infos.score += self.quiz.current_music.points
                        participant_infos.results[
                            self.quiz.playlist.index
                        ] = self.quiz.current_music.points

                        await ctx.send(
                            "You guessed right! You won %s points!"
                            % self.quiz.current_music.points
                        )

                        if self.quiz.settings.end_when_everyone_found:
                            # Check if the music should end if everyone guessed right
                            if self.participants.has_everyone_found(
                                self.quiz.playlist.index
                            ):
                                await self.write_to_participants_channels(
                                    "Everyone guessed right!"
                                )
                                await self.end_current_music()

            else:
                pass
        else:
            pass
