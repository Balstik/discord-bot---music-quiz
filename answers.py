from participant_infos import GuessDifficulty


class Answers:
    def __init__(
        self,
        exact_name: str,
        exact_author: str,
        exact_work: str,
        accepted_answers: dict,
    ):
        self.exact_name = exact_name
        self.exact_author = exact_author
        self.exact_work = exact_work
        self.accepted_answers = {}

        for (difficulty_level, answers) in accepted_answers.items():
            self.accepted_answers[GuessDifficulty[difficulty_level.upper()]] = answers

    def guess(
        self,
        guess: str,
        find: str = "author",
        difficulty: GuessDifficulty = GuessDifficulty.NORMAL,
    ) -> bool:
        guess = guess.lower()

        if find == "author" and self.exact_author:
            if guess == self.exact_author.lower():
                return True

        if find == "name" and self.exact_name:
            if guess == self.exact_name.lower():
                return True

        if find == "from" and self.exact_work:
            if guess == self.exact_work.lower():
                return True

        result = self.check_guess_for_difficulty(guess, find, difficulty)

        if result is not None:
            return True

        return False

    def check_guess_for_difficulty(
        self, guess: str, find: str, difficulty: GuessDifficulty
    ) -> GuessDifficulty:
        """Check if the specified guess is correct

        If no matching correct answers are found for the specified difficulty, 
        will check recursively for the higher difficulties

        returns the difficulty of the correct answer or None if the guess was not a correct answer
        """
        if guess in self.accepted_answers[difficulty][find]:
            return True

        difficulty += 1

        if difficulty < len(GuessDifficulty):
            return self.check_guess_for_difficulty(guess, find, difficulty)
        else:
            return None

