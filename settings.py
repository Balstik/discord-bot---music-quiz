class Settings:
    """
    order:  Specify in which order the playlist should be played
        normal :    use the order the music were specified in the source file
        random :    will choose a random music each time
        reverse :   use the reverse order of the source file
        points :    will choose musics awarding the least points first
    find:   Specify what information the users need to guess
        name :      the name of the music
        author :    the author of the music
        from :      the album or work of art the music is from
    music_time_type:  Specify how the duration of the songs should be chosen
        fixed :     Songs will always last the same (use the value in 'music_duration')
        specified : Songs will last the value in the yaml file for the music if one is specified, if not, it will use the value in 'music_duration'
    points_given_type: Specify how points are given to players who guess right
        same :      All players who guess right wil be given the same number of points
        fastest :   The first player to guess get the maximum number of points then the other get less points the longer they take to guess right   
    music_duration:  The duration a song will be played by default
    delay_between_musics:   The delay between a song ending and starting the next one
    end_when_everyone_found: Whether a music
    """

    find_questions = {
        "name": "What's this song name?",
        "author": "Who's the author of this song?",
        "from": "Where do this song come from?",
    }

    def __init__(self, infos: dict = None, order: str = "normal", find: str = "author"):
        # default values
        self.order: str = order
        self.find: str = find
        self.music_time_type: str = "fixed"
        self.points_given_type: str = "same"
        self.music_duration: float = 30
        self.delay_between_musics: int = 20
        self.end_when_everyone_found = True

        if infos is not None:
            self.order = infos.get("order", self.order)
            self.find = infos.get("find", self.find)
            self.music_time_type = infos.get("music_time_type", self.music_time_type)
            self.points_given_type = infos.get(
                "points_given_type", self.points_given_type
            )
            self.music_duration = float(
                infos.get("music_duration", self.music_duration)
            )
            self.delay_between_musics = float(
                infos.get("delay_between_musics", self.delay_between_musics)
            )
            self.end_when_everyone_found = infos.get(
                "end_when_everyone_found", self.end_when_everyone_found
            )

    def get_find_question(self, music: "Music") -> str:
        """ Returns a string representing the question to ask the players depending on 
        what the players have to find
        """
        find = ""
        if music:
            find = music.find
        else:
            find = self.find

        return Settings.find_questions.get(find, "")
