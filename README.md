Music Quiz Bot
==============

This bot allow you to create customized music quiz playlists and play them in your discord server.

The bot use YAML files as playlists data


How to use
----------

Use the command **!load** to load a specific quiz.

Use the command **!setup** to make the bot join your current channel.

Use the command **!start** to start the quiz.

Use the command **!stop** to stop the quiz.

Create playlists
----------------

### Playlist example

Playlists consists of YAML files as follow :

```yaml
quiz:
  name: A quiz
  description: 'The best quiz of all time'

settings: 
  order: random
  find: name
  music_time_type: fixed
  music_duration: 30
  delay_between_musics: 20

musics:
  - name: 'Never Gonna Give You Up'
    author: Rick Astley
    url: https://www.youtube.com/watch?v=dQw4w9WgXcQ
    points: 3
    accepted_answers:
      simplified:
        name: ['rick roll', rickroll]
```

### Playlist settings

The following arguments can be used to customize the quiz :

#### Quiz

**name**: the name of the quiz

**description**: the short description of the music in the quiz

#### Settings

**order**:  Specify in which order the playlist should be played

    normal:    use the order the music were specified in the source file (default)

    random:    will choose a random music each time

    reverse:   use the reverse order of the source file

    points:    will choose musics awarding the least points first

**find**:   Specify what information the users need to guess

    name:      the name of the music

    author:    the author of the music

**music_time_type**:  Specify how the duration of the songs should be chosen

    fixed:     Songs will always last the same (use the value in 'music_duration')

    specified: Songs will last the value in the yaml file for the music if one is specified, if not, it will use the value in 'music_duration'

**music_duration**:  The duration a song will be played by default

**delay_between_musics**:   The delay between a song ending and starting the next one

#### Music

**name**: the song name

**author**: the singer or band name

**url**: a direct url to the music source

**points**: the number of points awarded for guessing this song right

**accepted_answers**: a list of other acceptable answers in addition to the author and song name specified before.
Accepted asnwers can be set to 3 different difficulty levels : hard, normal and simplified.
Quiz participant set with simplified difficulty will be able to guess the answer from all the sets of accepted answers. Participants with normal difficulty can guess anwsers from hard and normal difficulty and so on.
