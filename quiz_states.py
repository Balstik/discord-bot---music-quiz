from quiz import Quiz
from participant_infos import Participants

import discord.utils as utils

from enum import Enum
import time
import asyncio

# from game_instance import GameInstance


class QuizStateType(Enum):
    STOPPED = 0  #
    LOADED = 1  # A quiz has been loaded
    READY = 2  # A quiz is loaded and the bot is ready
    PLAYING = 3  # A music is currently playing
    PAUSED = 4  # The quiz has been paused
    INTERVAL = 5  # The interval between two musics
    ENDED = 6  # the quiz is finished


class QuizState:
    transition_switch = {}

    def __init__(self, game_instance: "GameInstance"):
        self.game_instance: "GameInstance" = game_instance
        self.quiz: Quiz = self.game_instance.quiz
        self.participants: Participants = self.game_instance.participants

        # await self.on_state_enter()
        self.update_start = False

    async def on_state_enter(self, ctx=None, *args):
        pass

    async def on_state_update(self):
        pass

    async def on_state_exit(self, ctx=None, *args):
        pass

    @staticmethod
    async def none_func(self):
        pass

    async def transition_to(self, state: "QuizState"):
        """ Allow to run specific code during a transition between two chosen states
        """
        transition = self.transition_switch.get(
            self.game_instance.get_state_type(state), QuizState.none_func
        )

        await transition(self)

    def start_update(self):
        self.update_start = True


class PlayingState(QuizState):
    async def on_state_enter(self, ctx=None, *args):
        self.quiz.playing = True

        # load the first music
        if not self.quiz.playlist.next_music_loaded:
            await self.quiz.playlist.load_next()

        # retrieve next music and update playlist cursor
        self.quiz.current_music = self.quiz.playlist.get_next_music()

        if not self.quiz.playlist.next_music.loadable:
            # self.quiz.playlist.skip_to_next()
            # the next music have loading problem
            await self.game_instance.end_current_music()
            return

        # play the next music
        self.quiz.playlist.play_next()
        self.starting_time = time.time()
        self.delay_update = 1  # TODO: regroup this with delay update in IntervalState
        print("playing next music")
        print(self.quiz.current_music)

        self.left_string = "\n%ss left"
        self.starting_string = (
            "=== Playing next music ! You can start guessing ! This one is worth **%s** points. ===\n"
            % self.quiz.current_music.points
            + self.quiz.settings.get_find_question(self.quiz.current_music)
        )

        self.countdown_msgs = await self.game_instance.write_to_participants_channels(
            self.starting_string
            + self.left_string % int(self.quiz.current_music.duration)
        )

        self.quiz.current_music.load_image()

    async def on_state_update(self):
        if (time.time() - self.starting_time) > self.delay_update:
            print(self.quiz.current_music.duration - self.delay_update)

            # update all the countdown messages
            for msg in self.countdown_msgs:
                await msg.edit(
                    content=self.starting_string
                    + self.left_string
                    % int(self.quiz.current_music.duration - self.delay_update)
                )
            self.delay_update += 1
            # if the delay is reached, change to playing state
            if self.delay_update > self.quiz.current_music.duration:

                print("song duration %ss reached" % self.quiz.current_music.duration)
                # await self.game_instance.write_to_participants_channels("Time over!")
                await self.game_instance.end_current_music()

    async def on_state_exit(self, ctx=None, *args):
        # stop the music
        self.quiz.playlist.stop_current_music()

        if not self.quiz.playlist.next_music.loadable:
            print("Music from url %s could not be loaded" % self.quiz.current_music.url)
            await self.game_instance.write_to_participants_channels(
                "Music could not be loaded. Skipping..."
            )
            return

        for msg in self.countdown_msgs:
            await msg.edit(content=self.starting_string)

    async def reveal_music(self):
        # Do not reveal the music if it could not be loaded and played
        if not self.quiz.playlist.next_music.loadable:
            return

        await self.game_instance.write_to_participants_channels(
            ("The song was%s") % (self.quiz.current_music.get_answer_format()),
            self.quiz.current_music.image_file,
        )
        await self.game_instance.write_to_participants_channels(
            ("Current leaderboards :\n" + self.participants.get_score_str())
        )

    transition_switch = {
        QuizStateType.INTERVAL: reveal_music,
        QuizStateType.ENDED: reveal_music,
    }


class PausedState(QuizState):
    pass


class IntervalState(QuizState):
    async def on_state_enter(self, ctx=None, *args):
        self.pause_delay = self.quiz.settings.delay_between_musics
        self.delay_update = 1
        self.delay_string = "%ss left before next music"
        self.countdown_msgs = await self.game_instance.write_to_participants_channels(
            self.delay_string % int(self.pause_delay)
        )
        self.starting_time = time.time()
        print("waiting %ss before next music" % self.pause_delay)

        # loading next music during the pause
        self.next_music_loading = asyncio.create_task(self.quiz.playlist.load_next())

    async def on_state_update(self):
        # while the delay has not been reached
        if (time.time() - self.starting_time) > self.delay_update:
            print(self.pause_delay - self.delay_update)

            # update all the countdown messages
            for msg in self.countdown_msgs:
                await msg.edit(
                    content=self.delay_string
                    % int(self.pause_delay - self.delay_update)
                )
            self.delay_update += 1
            # if the delay is reached, change to playing state
            if self.delay_update > self.pause_delay:
                await self.game_instance.change_state_to(
                    PlayingState(self.game_instance)
                )

    async def on_state_exit(self, ctx=None, *args):
        for msg in self.countdown_msgs:
            await msg.delete()
        await self.next_music_loading


class StoppedState(QuizState):
    async def on_state_enter(self, ctx=None, *args):
        voice_client = utils.get(
            self.game_instance.bot.voice_clients, guild=self.game_instance.guild
        )
        self.quiz.stop()
        print("stopping the quiz")
        if voice_client is not None:
            await voice_client.disconnect()


class LoadedState(QuizState):
    async def on_state_enter(self, ctx=None, *args):
        self.game_instance.quiz = Quiz()
        self.quiz = self.game_instance.quiz
        filename = "test.yaml"

        if len(ctx.message.attachments) >= 1:
            att = ctx.message.attachments[0]
            content = await att.read()
            self.quiz.load_from_bytes(content)
        else:
            self.quiz.load_from_file(filename)

        await ctx.send(
            "Loaded %s with %s musics" % (self.quiz.name, self.quiz.playlist.nb_music)
        )


class ReadyState(QuizState):
    async def on_state_enter(self, ctx=None, *args):
        # TODO : maybe move voice_client elsewhere ?

        self.game_instance.quiz.playlist.voice_client = (
            await ctx.author.voice.channel.connect()
        )
        print("Bot joined voice channel")

        participants = ctx.author.voice.channel.members
        await self.game_instance.participants.add_and_setup_participants(participants)


class EndedState(QuizState):
    async def on_state_enter(self, ctx=None, *args):
        await self.game_instance.write_to_participants_channels(
            "Quiz is finished! Thanks for playing!"
        )

