from discord import TextChannel, Member, Guild
from discord import PermissionOverwrite

from enum import IntEnum
import operator
import discord.utils as utils


from typing import Dict, List

from tools import Tools


class GuessDifficulty(IntEnum):
    SIMPLIFIED = 1
    NORMAL = 2
    HARD = 3


class ParticipantInfos:
    def __init__(self):
        self.difficulty: GuessDifficulty = GuessDifficulty.NORMAL
        self.answer_channel: TextChannel = None
        # results = {<music index>: <points>}, music index is needed if playlist is played in random order
        self.results: Dict[int, int] = {}

    def total_score(self):
        total = 0
        for points in self.results.values():
            total += points
        return total

    def __lt__(self, participant_infos: "ParticipantInfos"):
        return self.total_score() < participant_infos.total_score()


class Participants:
    def __init__(self, participants_infos: Dict[Member, ParticipantInfos] = None):
        if participants_infos is None:
            participants_infos = {}

        self.participants_infos: Dict[Member, ParticipantInfos] = participants_infos
        self.answer_channel_category_name: str = "Answer Channels"
        self.guild: Guild = None

    def __contains__(self, item) -> bool:
        return item in self.participants_infos

    def get_infos(self, participant: Member) -> ParticipantInfos:
        return self.participants_infos[participant]

    def get_score_str(self) -> str:
        score_str = ""
        sorted_infos = sorted(
            self.participants_infos.items(), key=operator.itemgetter(1), reverse=True
        )
        for (participant, infos) in sorted_infos:
            score_str += participant.name + " - " + str(infos.total_score()) + "\n"
        return score_str

    def get_answer_channels(self) -> List[TextChannel]:
        channels = []
        for infos in self.participants_infos.values():
            channels.append(infos.answer_channel)
        return channels

    async def add_and_setup_participants(self, participants: List[Member]):
        """ Create and store participant infos for all specified participants
        and set up the answer category and answer channels for all of them if needed.
        """
        # remove bots from participants
        for i in range(len(participants)):
            if i >= len(participants):
                break
            if participants[i].bot:
                del participants[i]

        # store participants infos
        for participant in participants:
            # do not reset infos if participant is already in
            # if participant not in self.participants_infos:
            if not self.participants_infos.get(participant):
                self.participants_infos[participant] = ParticipantInfos()

        # retrieve channel category if it already exists and if not, create a category
        answer_category = await Tools.get_or_create_category(
            self.guild, self.answer_channel_category_name
        )

        # answer_category = utils.get(
        #     self.guild.categories, name=self.answer_channel_category_name
        # )
        self.answer_channel_category_id = answer_category.id

        # # if not, create a category
        # if answer_category is None:
        #     answer_category = await self.guild.create_category(
        #         self.answer_channel_category_name
        #     )
        #     self.answer_channel_category_id = answer_category.id

        # for each participant
        # for participant in self.participants_infos.keys():
        for participant in participants:
            text_channel_name = "answers-" + str(participant.id)

            overwrites = {
                self.guild.default_role: PermissionOverwrite(read_messages=False),
                self.guild.me: PermissionOverwrite(read_messages=True),
                participant: PermissionOverwrite(read_messages=True),
            }

            # retrieve or create new channel
            answer_channel = await Tools.get_or_create_TextChannel(
                answer_category, text_channel_name, overwrites
            )
            # # if a channel has already been created for them
            # answer_channel = utils.get(
            #     answer_category.text_channels, name=text_channel_name
            # )

            # # if not, create the text channel
            # if answer_channel is None:
            #     overwrites = {
            #         self.guild.default_role: PermissionOverwrite(read_messages=False),
            #         self.guild.me: PermissionOverwrite(read_messages=True),
            #         participant: PermissionOverwrite(read_messages=True),
            #     }
            #     answer_channel = await self.guild.create_text_channel(
            #         text_channel_name, overwrites=overwrites, category=answer_category
            #     )
            self.participants_infos[participant].answer_channel = answer_channel

    def has_everyone_found(self, music_index: int) -> bool:
        """ Check if every participant found the answer for the music 
        with the specified index
        """
        return self.get_nb_player_found(music_index) == len(self.participants_infos)
        # for infos in self.participants_infos.values():
        #     if not infos.results.get(music_index):
        #         return False
        # return True

    def get_nb_player_found(self, music_index: int) -> int:
        """ Returns the number of player who guessed right for the music 
        with the specified index
        """
        nb_player = 0
        for infos in self.participants_infos.values():
            if infos.results.get(music_index):
                nb_player += 1

        return nb_player
