from discord import FFmpegPCMAudio, PCMVolumeTransformer

from sort import random_sort, points_sort
from settings import Settings
from music import Music


class Playlist:
    def __init__(self, music_data: list = None, settings: Settings = Settings()):
        self.voice_client = None

        self.musics = []
        self.index = 0
        self.nb_music = 0
        self.music_left = True

        self.next_stream_url = None
        self.next_music_loaded: bool = False
        self.next_music: Music = None
        if music_data is not None:

            for music_infos in music_data:
                music = Music(music_infos, settings)
                self.musics.append(music)
            if settings.order == "random":
                self.musics = sorted(self.musics, key=random_sort)
            elif settings.order == "points":
                self.musics = sorted(self.musics, key=points_sort)
            elif settings.order == "reversed":
                self.musics = sorted(self.musics, reverse=True)
            self.nb_music = len(self.musics)
            print(self.musics)

    async def load_next(self) -> bool:
        """
        Returns True if the next music has been successfully loaded
        """
        if len(self.musics) >= 1:
            if self.music_left:
                next_music = self.musics[self.index]
                self.next_music = next_music
                self.next_stream_url = next_music.load_music()
                if self.next_stream_url is None:
                    return False
                self.next_music_loaded = True
                return True
            else:
                print("No music left to load next")
        else:
            print("Error : No musics currently imported")

    def play_next(self):
        if self.next_music_loaded:
            # next_music = self.musics[self.index]
            # stream_url = next_music.load_music()
            # TODO: set volume
            source = PCMVolumeTransformer(FFmpegPCMAudio(self.next_stream_url), 0.1)
            if self.voice_client.is_connected():
                self.voice_client.play(source)
            self.next_music_loaded = False
        else:
            print("next music has not been loaded yet")
        return None

    def get_next_music(self) -> Music:
        self.move_forward()
        return self.next_music

    def move_forward(self):
        self.index += 1
        # TODO: reset next music and next url to None
        if self.index >= self.nb_music:
            self.music_left = False

    def stop_current_music(self):
        # TODO : just stop the current music
        self.voice_client.stop()

    def stop_playlist(self):
        if self.voice_client is not None:
            self.voice_client.stop()
        self.reset()

    def reset(self):
        self.index = 0
        self.music_left = True

